var peoples = [];
var filteredPeople = [];
window.addEventListener('load', () => {
  loadArrayPeoples();
  //window.addEventListener('input', searchUser);
});

async function loadArrayPeoples() {
  let result = await fetch(
    'https://randomuser.me/api/?seed=javascript&results=100&nat=BR&noinfo'
  );
  peoples = await result.json();
  peoples = peoples.results.map((person) => {
    return {
      name: `${person.name.first} ${person.name.last}`,
      picture: person.picture,
      age: person.dob.age,
      gender: person.gender,
    };
  });
  filteredPeople = peoples;
  console.log(filteredPeople);
}
/**
 * Realiza  busca dos usuários pega o valor digitado no search e utiliza o
 * includes - verifica se o array contém determinado elemento
 * toLowerCase() -  deixa todas em minuscula, assim o usuário pode digitar maisc ou minusc a busca será realiada corretamente
 */
function searchUser() {
  let name = document.getElementById('search').value;
  filteredPeople = peoples;

  //se nome não estiver vazio entra
  if (name) {
    filteredPeople = filteredPeople.filter((x) =>
      x.name.toLowerCase().includes(name.toLowerCase())
    );
    //Se a busca  não achar elementos entra aqui e retorna
    if (filteredPeople.length <= 0) {
      clean();
      return;
    }

    //se encontrar elementos faz isso aqui
    createUl();
    createStatistic();
  } else {
    clean();
  }
  console.log(filteredPeople.length);
}

/**
 * Cria os componentes para apresentar na tela
 */
function createUl() {
  let peopleDiv = document.getElementById('listPeoplesId');
  peopleDiv.innerHTML = '';
  let ul = document.createElement('ul');
  let h3 = document.createElement('h3');
  h3.textContent = `${filteredPeople.length} usuário(s) encontrado(s)`;
  ul.appendChild(h3);
  filteredPeople.forEach((res) => {
    let li = document.createElement('li');
    let image = document.createElement('img');
    image.src = `${res.picture.medium}`;

    let span = document.createElement('span');
    span.textContent = `${res.name}, ${res.age} anos`;

    li.appendChild(image);
    li.appendChild(span);
    li.classList.add('chip');
    ul.appendChild(li);
  });

  peopleDiv.appendChild(ul);
}

function clean() {
  let peopleDiv = document.getElementById('listPeoplesId');
  peopleDiv.innerHTML = ' Nenhum usuário filtrado';

  let resumeDiv = document.getElementById('resumePeoplesId');
  resumeDiv.innerHTML = '  Nada a ser exibido';
}

function createStatistic() {
  let sumAgeMale = getSumAge('male');
  let sumAgeFemale = getSumAge('female');
  let sumAges = getSumAllAges();
  let media = (sumAges / filteredPeople.length).toFixed(2);

  let resumeDiv = document.getElementById('resumePeoplesId');
  resumeDiv.innerHTML = '';

  let ul = document.createElement('ul');
  let liAgeMale = document.createElement('li');
  let liAgeFemale = document.createElement('li');
  let liSumAges = document.createElement('li');
  let liMedia = document.createElement('li');
  let h3 = document.createElement('h3');

  liAgeMale.textContent = `Sexo masculino ${sumAgeMale}`;
  liAgeFemale.textContent = `Sexo feminino ${sumAgeFemale}`;
  liSumAges.textContent = `Soma das idades ${sumAges}`;
  liMedia.textContent = `Média das idades ${media}`;

  h3.textContent = `Estatísticas`;
  ul.appendChild(h3);
  ul.appendChild(liAgeMale);
  ul.appendChild(liAgeFemale);
  ul.appendChild(liSumAges);
  ul.appendChild(liMedia);
  resumeDiv.appendChild(ul);
}

function getSumAge(genderString) {
  return filteredPeople.reduce((accumultor, people) => {
    return accumultor + (people.gender === genderString);
  }, 0);
}

function getSumAllAges() {
  return filteredPeople.reduce((accumultor, people) => {
    return accumultor + people.age;
  }, 0);
}
